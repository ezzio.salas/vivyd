// commit 1
import "./App.css";
import Header from "./components/header";
import Movie from "./components/movie";
function App() {
  return (
    <main className="container">
      <Header></Header>
      <Movie></Movie>
    </main>
  );
}

export default App;
