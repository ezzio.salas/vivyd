//commit one
//commit two
import React, { Component } from "react";
import { getMovies } from "../services/fakeMovieService";
class Movie extends Component {
  state = { movies: getMovies() };

  handleDelete = (movie) => {
    const movies = this.state.movies.filter((m) => m._id !== movie._id);
    this.setState({ movies: movies });
  };
  render() {
    const { length: count } = this.state.movies;
    if (count === 0)
      return (
        <div className="alert alert-danger" role="alert">
          <p>We ran out of movies!!</p>
        </div>
      );

    return (
      <React.Fragment>
        <div className="alert alert-primary" role="alert">
          <p>Showing {count} movies in the stock</p>
        </div>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Titles.</th>
              <th scope="col">Genre.</th>
              <th scope="col">Stock</th>
              <th scope="col">Rate</th>
            </tr>
          </thead>
          <tbody>
            {this.state.movies.map((movie) => (
              <tr key={movie._id}>
                <td>{movie.title}</td>
                <td>{movie.genre.name}</td>
                <td>{movie.numberInStock}</td>
                <td>{movie.dailyRentalRate}</td>
                <td>
                  <button
                    onClick={() => {
                      this.handleDelete(movie);
                    }}
                    className="btn btn-danger sm-2"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

export default Movie;
